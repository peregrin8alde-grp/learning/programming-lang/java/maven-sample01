# maven-sample01

Maven を使った開発のサンプル。

VSCode + devcontainer を使った開発を想定

- Java / Maven の Docker イメージ : https://hub.docker.com/_/maven


## Maven コンテナの実行ユーザについて

Docker イメージ公式でサンプルとして記載されている以下をもとに、非 root ユーザで実行する。

```
$ docker run -v ~/.m2:/var/maven/.m2 -ti --rm -u 1000 -e MAVEN_CONFIG=/var/maven/.m2 maven mvn -Duser.home=/var/maven archetype:generate
```

- `user.home` という Java プロパティ指定が必要
- Docker ホスト側のユーザに合わせて、 1000:1000 で実行
- Maven リポジトリはプロジェクト配下に maven というディレクトリを作成して利用させる。
  - 生産物の一部として、 git 管理外

## 設定ファイルの作成

参考 : https://maven.apache.org/settings.html

```
mkdir -p maven/.m2

tee maven/.m2/settings.xml <<EOF > /dev/null
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 https://maven.apache.org/xsd/settings-1.0.0.xsd">
  <localRepository/>
  <interactiveMode/>
  <offline/>
  <pluginGroups/>
  <servers/>
  <mirrors/>
  <proxies/>
  <profiles/>
  <activeProfiles/>
</settings>
EOF
```


## プロジェクト作成

シンプル構成の Maven プロジェクトで開始する。

参考 : 

- https://maven.apache.org/archetypes/
- https://maven.apache.org/archetype/maven-archetype-plugin/generate-mojo.html
- https://maven.apache.org/archetype/maven-archetype-plugin/index.html

最新の Java や JUnit を使うのであれば maven-archetype-quickstart を使った方が良さそう。

参考

- https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html


### 初期化

```
mkdir -p prj

GRPID=com.mycompany.app
ARTIFACTID=my-app

docker run \
  -it --rm \
  -u 1000:1000 \
  -v "$(pwd)/prj":/prj \
  -v "$(pwd)/maven":/maven \
  -w /prj \
  -e MAVEN_CONFIG=/maven/.m2 \
  maven:3.9 \
    mvn archetype:generate \
      -Duser.home=/maven \
      -DgroupId=${GRPID} \
      -DartifactId=${ARTIFACTID} \
      -DarchetypeArtifactId=maven-archetype-quickstart \
      -DarchetypeVersion=1.5 \
      -DinteractiveMode=false
```

### Java バージョン関連の設定

pom.xml の編集。
ここでは Java11 を使う想定とする。

```
        <properties>
            <maven.compiler.release>11</maven.compiler.release>
        </properties>
     
        <build>
            <pluginManagement>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-compiler-plugin</artifactId>
                        <version>3.8.1</version>
                    </plugin>
                </plugins>
            </pluginManagement>
        </build>
```

maven-archetype-quickstart で作成した場合は元々こうなっているため編集不要だと思われる

### 動作確認

ビルド

```
docker run \
  -it --rm \
  -u 1000:1000 \
  -v "$(pwd)/prj/${ARTIFACTID}":/prj \
  -v "$(pwd)/maven":/maven \
  -w /prj \
  -e MAVEN_CONFIG=/maven/.m2 \
  maven:3.9 \
    mvn package \
      -Duser.home=/maven
```

実行

```
docker run \
  -it --rm \
  -u 1000:1000 \
  -v "$(pwd)/prj/${ARTIFACTID}":/prj \
  -v "$(pwd)/maven":/maven \
  -w /prj \
  -e MAVEN_CONFIG=/maven/.m2 \
  maven:3.9 \
    java -cp target/my-app-1.0-SNAPSHOT.jar com.mycompany.app.App
```

java コマンドでの実行時に依存ライブラリを使いたい場合

依存ライブラリのダウンロード（デフォルトのダウンロード先は `target/dependency` ）付きビルド

```
docker run \
  -it --rm \
  -u 1000:1000 \
  -v "$(pwd)/prj/${ARTIFACTID}":/prj \
  -v "$(pwd)/maven":/maven \
  -w /prj \
  -e MAVEN_CONFIG=/maven/.m2 \
  maven:3.9 \
    mvn clean dependency:copy-dependencies package \
      -Duser.home=/maven
```

依存ライブラリにもクラスパスを通して実行

```
docker run \
  -it --rm \
  -u 1000:1000 \
  -v "$(pwd)/prj/${ARTIFACTID}":/prj \
  -v "$(pwd)/maven":/maven \
  -w /prj \
  -e MAVEN_CONFIG=/maven/.m2 \
  maven:3.9 \
    java -cp target/my-app-1.0-SNAPSHOT.jar:target/dependency/* com.mycompany.app.App
```


## .devcontainer の作成

実行ユーザや Maven リポジトリなどに注意して各ファイルを作成


## コンテナ内での開発作業

コンテナ内の作業用ディレクトリでは以下のコマンドでビルドや実行などができる。
（ VSCode の Maven プラグインを使った作業も可）

```
# クリーンしてビルド（ jar 作成）
mvn clean package -f prj01/pom.xml

# main の実行
mvn exec:java -Dexec.mainClass=sample.prj01.App -f prj01/pom.xml
```



